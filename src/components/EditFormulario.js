import React, { useState } from 'react';

const EditFormulario = ({ dato, listUpdate, setState, actualizado }) => {
  const [udpateCita, setUpdateCita] = useState(listUpdate);
  const { pet, owner, fecha, time, symptoms } = listUpdate;

  const handleUpdatePet = (e) => {
    setUpdateCita({ ...udpateCita, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    actualizado(udpateCita.id, udpateCita);
    setState(true);
  };
  return (
    <div className="formulario">
      <h2>{dato}</h2>
      <form onSubmit={handleSubmit}>
        <label>Nombre de Mascota</label>
        <input
          type="text"
          name="pet"
          className="u-full-width"
          placeholder="Nombre de Mascota"
          onChange={handleUpdatePet}
          defaultValue={pet}
        />
        <label>Nombre del Dueño</label>
        <input
          type="text"
          name="owner"
          className="u-full-width"
          placeholder="Nombre del Dueño"
          onChange={handleUpdatePet}
          defaultValue={owner}
        />
        <label>Fecha</label>
        <input
          type="date"
          name="fecha"
          className="u-full-width"
          onChange={handleUpdatePet}
          defaultValue={fecha}
        />
        <label>Hora</label>
        <input
          type="time"
          name="time"
          className="u-full-width"
          onChange={handleUpdatePet}
          defaultValue={time}
        />
        <label>Sintomas</label>
        <textarea
          name="symptoms"
          className="u-full-width"
          onChange={handleUpdatePet}
          defaultValue={symptoms}
        ></textarea>
        <button type="submit" className="u-full-width button-primary">
          {dato}
        </button>
      </form>
    </div>
  );
};

export default EditFormulario;
