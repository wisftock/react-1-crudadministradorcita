import React from 'react';
import PropTypes from 'prop-types';

const Cita = ({ cita, deleteCita, editCita }) => {
  const { id, pet, owner, fecha, time, symptoms } = cita;
  return (
    <div className="cita animate__animated animate__fadeIn">
      <p>
        Mascota <span>{pet}</span>
      </p>
      <p>
        Dueño <span>{owner}</span>
      </p>
      <p>
        Fecha <span>{fecha}</span>
      </p>
      <p>
        Hora <span>{time}</span>
      </p>
      <p>
        Sintomas <span>{symptoms}</span>
      </p>
      <button onClick={() => editCita(cita)} className="editar">
        Editar
      </button>{' '}
      <button onClick={() => deleteCita(id)} className="eliminar">
        Eliminar
      </button>
    </div>
  );
};
Cita.propTypes = {
  cita: PropTypes.object.isRequired,
  deleteCita: PropTypes.func.isRequired,
  editCita: PropTypes.func.isRequired,
};
export default Cita;
