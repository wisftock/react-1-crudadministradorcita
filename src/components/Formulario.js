import React, { useState } from 'react';
import Mensaje from './Mensaje';

const Formulario = ({ createCita }) => {
  const initialState = {
    id: '',
    pet: '',
    owner: '',
    fecha: '',
    time: '',
    symptoms: '',
  };
  const [cita, setCita] = useState(initialState);
  const [mensaje, setMensaje] = useState(false);
  const handleUpdatePet = (e) => {
    setCita({ ...cita, [e.target.name]: e.target.value });
  };

  const { pet, owner, fecha, time, symptoms } = cita;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      pet.trim() === '' ||
      owner.trim() === '' ||
      fecha.trim() === '' ||
      time.trim() === '' ||
      symptoms.trim() === ''
    ) {
      setMensaje(true);
      setTimeout(() => {
        setMensaje(false);
      }, 2000);
      console.log('los campos no deben ir vacios');
      return;
    }
    cita.id = Date.now();
    createCita(cita);
    // resetear
    setCita(initialState);
  };
  return (
    <div className="formulario">
      <h2>Crear Cita</h2>
      {mensaje && <Mensaje message="Todos los campos son obligatorios" />}
      <form onSubmit={handleSubmit}>
        <label>Nombre de Mascota</label>
        <input
          type="text"
          name="pet"
          className="u-full-width"
          placeholder="Nombre de Mascota"
          onChange={handleUpdatePet}
          value={pet}
        />
        <label>Nombre del Dueño</label>
        <input
          type="text"
          name="owner"
          className="u-full-width"
          placeholder="Nombre del Dueño"
          onChange={handleUpdatePet}
          value={owner}
        />
        <label>Fecha</label>
        <input
          type="date"
          name="fecha"
          className="u-full-width"
          onChange={handleUpdatePet}
          value={fecha}
        />
        <label>Hora</label>
        <input
          type="time"
          name="time"
          className="u-full-width"
          onChange={handleUpdatePet}
          value={time}
        />
        <label>Sintomas</label>
        <textarea
          name="symptoms"
          className="u-full-width"
          onChange={handleUpdatePet}
          value={symptoms}
        ></textarea>
        <button type="submit" className="u-full-width button-primary">
          Agregar cita
        </button>
      </form>
    </div>
  );
};

export default Formulario;
