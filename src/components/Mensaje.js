import React from 'react';
import PropTypes from 'prop-types';

const Mensaje = ({ message }) => {
  return (
    <div>
      <p className="alerta-error">{message}</p>
    </div>
  );
};
Mensaje.propTypes = {
  message: PropTypes.string.isRequired,
};
export default Mensaje;
