import React, { Fragment, useState } from 'react';
import Cita from './components/Cita';
import EditFormulario from './components/EditFormulario';
import Formulario from './components/Formulario';

function App() {
  const [state, setState] = useState(true);
  const [listCitas, setListCitas] = useState([]);

  const createCita = (cita) => {
    setListCitas([...listCitas, cita]);
  };
  const deleteCita = (id) => {
    const listDelete = listCitas.filter((cita) => cita.id !== id);
    setListCitas(listDelete);
  };

  const [listUpdate, setListUpdate] = useState([]);
  const editCita = (cita) => {
    setListUpdate({
      id: cita.id,
      pet: cita.pet,
      owner: cita.owner,
      fecha: cita.fecha,
      time: cita.time,
      symptoms: cita.symptoms,
    });

    setState(false);
  };
  const actualizado = (id, data) => {
    const newList = listCitas.map((datos) => (datos.id === id ? data : datos));
    setListCitas(newList);
  };
  const titulo =
    listCitas.length === 0 ? 'No hay citas' : 'Administra tus citas';
  return (
    <Fragment>
      <h1>Administrador de Pacientes</h1>
      <div className="container">
        <div className="row">
          <div className="one-half column">
            {state ? (
              <Formulario
                createCita={createCita}
                className="formulario"
                dato="Crear cita"
              />
            ) : (
              <EditFormulario
                dato="Editar cita"
                listUpdate={listUpdate}
                setState={setState}
                actualizado={actualizado}
              />
            )}
          </div>
          <div className="one-half column">
            <div className="listacitas">
              <h2>{titulo}</h2>
              {listCitas.map((cita) => {
                return (
                  <Cita
                    key={cita.id}
                    cita={cita}
                    deleteCita={deleteCita}
                    editCita={editCita}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
